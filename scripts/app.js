var fnlog = function(a) {
	console.log(a);
};

var searchW = new Worker("scripts/workers/searchWorker.js");
searchW.addEventListener('message', function(e)
{
	var as = e.data.as;
	var mal = e.data.mal;

	var row = core.addRow(mal, as);
	tableres.appendChild(row);

}, false);

var core = {
	createEl: function(name, data)
	{
		var el = document.createElement(name);
		for (var key in data)
		{
			if(data.hasOwnProperty(key))
			{
				el[key] = data[key];
			}
		}
		return el;
	},
	// Zeile zur Ergebnistabelle hinzufg.
	addRow: function(maldata, asdata)
	{
		var row = document.createElement("tr");

		row.dataset.userdata = JSON.stringify({
			asId: '3733',
			count: maldata.my_watched_episodes,
			type: maldata.my_status,
			rating: maldata.my_score,
			rewatch: '0' // TODO
		});

		var colMAL = core.createEl("td", {innerText: maldata.title});
		colMAL.addEventListener("click", function () {
			console.log(maldata, asdata);
		});

		row.appendChild(colMAL);
		row.appendChild(core.createEl("td", {innerText: maldata.series_animedb_id}));

		var sel = document.createElement("select");
		if(asdata.length > 1)
			sel.className = "red";
		if(asdata.length == 0)
			sel.className = "notfound";

		var save = false;

		var highest = 0;
		var highestOpt;

		var option = core.createEl("option", {value: ''});
		sel.appendChild(option);

		for (var i = 0; i < asdata.length; i++)
		{
			var option = core.createEl("option", {value: asdata[i]['link']});
			
			
			var points = 0;

			if(maldata.series_title == asdata[i]['name'])
				points += 3;
			// elseif alternative title +2 punkte

			if(asdata[i]['count'] == maldata.series_episodes)
				points += 1;
			else
				points -= 1;

			if(asdata[i]['year'] == (new Date(maldata.series_start)).getFullYear())
				points += 1;
			else
				points -= 1;

			if(points > highest)
			{
				highest = points;
				highestOpt = option;
			}
			/*
			NAME exakt gleich +3 Punkte
			Jahr gleich +1 | -1
			EPs gleich +1 | -1

			=> max 5 Punkte
			=> wenn der name gleich ist hat er immer höhere warscheinlichkeit richtig zu sein als nur jahr und episoden
			*/

			option.innerText = asdata[i]['name'] + ' '+ asdata[i]['year'];

			sel.appendChild(option);
		}
		if(highestOpt && highest > 0)
		{
			highestOpt.setAttribute("selected", "selected");
			if(highest > 4)
				sel.className = "almost";
			else if(highest > 2)
				sel.className = "maybe";
		}
		
		var colAS = document.createElement("td");
		colAS.appendChild(sel);

		var searchButton = document.createElement("button");
		searchButton.setAttribute("type", "button");
		searchButton.innerText = "Q";

		searchButton.addEventListener('click', function (){

			chrome.app.window.create('browser.html', {
				'bounds': {
					'width': 1000,
					'height': 700
				}
			}, function(createdWindow){
				createdWindow.contentWindow.asdata = asdata;
				createdWindow.contentWindow.maldata = maldata;
				createdWindow.contentWindow.row = row;
			});

		});
		
		colAS.appendChild(searchButton);

		row.appendChild(colAS);
		return row;

	},
	// Laden durch enter
	enterstart: function(e) {
		if (e.keyCode == 13) {
			core.start();
		}
	},
	// Laden
	start: function() {
		var user = document.querySelector("#mal-username").value;
		var table = document.querySelector("table tbody");

		if(user.value != "")
		{
			table.innerHTML = "";

			malApi.getAnimeList(user, function(items){
				for (var i = 0; i < items.myanimelist.anime.length; i++)
				{
					var item = items.myanimelist.anime[i];

					item.title = item.series_title;


					searchW.postMessage(item);				
				}
			});

		}
	},
	// Speichern
	save: function() {
		var user = document.querySelector("#aS-username");
		var pass = document.querySelector("#aS-password");

		if(user.value == '' || pass.value == '')
			return;

		aSApi.login(user.value, pass.value, function(status) {
			if(status == 'success')
			{
				var regex = /(?:.*)\.anisearch\.com\/anime\/([0-9]*),(.*)/;
				var rows = document.querySelectorAll("table.results tbody tr");
				for (var i = 0; i < rows.length; i++)
				{
					var asselected = rows[i].querySelector("select option:checked");
					if(asselected.value != '')
					{
						var match = regex.exec(asselected.value);
						if(match[1])
						{
							var userdata = JSON.parse(rows[i].dataset.userdata);
							aSApi.updateRating(userdata, function(data) {
								var status = JSON.parse(data.response);
								if(status.status  != "error")
								{
									rows[i].remove();
								}
							});
						}
					}
				}
			}
		});
		// TODO:
		// mal, aS ID's auf Server speichern für besseren Abgleichindex
	}
};

document.addEventListener('DOMContentLoaded', function() {
	document.querySelector("#load-btn").addEventListener('click', core.start);
	document.querySelector("#save-btn").addEventListener('click', core.save);

	document.querySelector("#mal-username").addEventListener('keydown', core.enterstart);
	window.tableres = document.querySelector("table tbody");
});
