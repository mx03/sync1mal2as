var httpClient = {
	get: function(url, fn) {
		var xhr = new XMLHttpRequest();

		xhr.open('GET', url);
		xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

		xhr.onreadystatechange = function(e)
		{
			if (this.readyState == 4 && this.status == 200)
			{
				fn(this);
			}
		};

		xhr.send();
	},
	post: function(url, data, fn)
	{
		var xhr = new XMLHttpRequest();

		xhr.open('POST', url);
		xhr.setRequestHeader('X-Requested-With','XMLHttpRequest');

		xhr.onreadystatechange = function(e)
		{
			if (this.readyState == 4 && this.status == 200)
			{
				fn(this);
			}
		};

		xhr.send(data);
	}
};