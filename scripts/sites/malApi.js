var malApi = {
	search: function(value, type, fn) {
		httpClient.get("http://myanimelist.net/api/"+type+"/search.xml?q="+value, function(xhr) {
			var x2js = new X2JS();
			var items = x2js.xml_str2json(xhr.response);

			fn(items);
		});
	},
	getAnimeList: function(username, fn)
	{
		httpClient.get("http://myanimelist.net/malappinfo.php?u="+username+"&status=all&type=anime", function(xhr) {
			var x2js = new X2JS();
			var items = x2js.xml_str2json(xhr.response);

			fn(items);
		});
	},
	getMangaList: function(username, fn)
	{
		httpClient.get("http://myanimelist.net/malappinfo.php?u="+username+"&status=all&type=manga", function(xhr) {
			var x2js = new X2JS();
			var items = x2js.xml_str2json(xhr.response);

			fn(items);
		});
	}
};