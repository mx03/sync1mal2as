var aSApi = {
	login: function(username, password, fn)
	{
		var regexpToken = /^var token = '(.*)';$/m;

		var data = new FormData();
		data.append('email', username);
		data.append('password', password);
		data.append('remember', '0');
		data.append('submit', 'Anmelden');

		httpClient.post('http://de.anisearch.com/login', data, function(data) {
			// TODO if login successfull
			httpClient.get('http://de.anisearch.com/usercp/list/anime/?view=3&limit=40', function(data2) {
				var match = regexpToken.exec(data2.response);
				if(match[1])
				{
					aSApi['token'] = match[1];
					fn('success');
				}
				else
				{
					fn('error');
				}
			});
		});
	},
	updateRating: function(data, fn) {
		if(typeof aSApi.token === 'undefined')
			fn('error');
			/*
			type(gleich wie bei mal): 
			0
			1: Angefangen
			2: Abgeschlossen
			3: Pausiert
			4: Abgebrochen
			5: Desinteressiert
			6. Lesezeichen

			rating(gleich wie bei mal): 0-10
			*/
		var submitData = new FormData();
		submitData.append('d[e]', data.count); // Episodes
		submitData.append('d[t]', data.type); // angefangen,abgeschlossen etc status
		submitData.append('d[r]', data.rating); // rating
		submitData.append('d[p]', '0'); // position
		submitData.append('d[w]', data.rewatch); // rewatch
		submitData.append('d[x]', '0'); // unsichtbar(secret) 0, 1
		submitData.append('d[z]', '1'); // aenderungsdatum aktualisieren
		submitData.append('d[n]', ''); // Notiz
		submitData.append('d[d]', 'xx'); // Dub
		submitData.append('d[s]', 'xx'); // Sub
		submitData.append('i', data.asId); // aniSearch ID
		submitData.append('s', aSApi.token); // token

		httpClient.post('http://de.anisearch.com/ajax/usercp/list/anime/episode', submitData, function(xhr) {
			fn(xhr);
		});
		
	},
	// depricated -> searchWorker
	search: function(dataobj, type, fn) {
		if(typeof dataobj === 'object')
			var value = dataobj.title;
		else
			var value = dataobj;

		var data = new FormData();
		data.append('t', value);
		data.append('p', '0');

		httpClient.post('http://de.anisearch.com/ajax/search/'+type, data, function(xhr) {

			var regex = /([0-9]*|\?) \(([0-9]*)\)/; 
			var items = [];
			var resData = JSON.parse(xhr.response);
			for (var i = 0; i < resData.r.length; i++) {
				var entry = resData.r[i];

				if(entry.i !== "")
				{
					var match = regex.exec(entry.v);
					var anzahl = match[1];
					var jahr = match[2];

					items.push({
						id: entry.i,
						count: anzahl,
						year: jahr,
						title: entry.t,
						urlpart: entry.u
					});
				}
			}
			fn(items, dataobj);
		});
	}
};